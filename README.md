![stability-wip](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![issues-open](https://bitbucket.org/repo/ekyaeEE/images/2944199103-issues_open.png)

# RATIONALE #

Artificial intelligence applied on digitized documentation
![OCR.jpg](https://bitbucket.org/repo/8zyLxnx/images/2030017620-OCR.jpg)

### What is this repository for? ###

* Quick summary
    - Good practices on processing digital historical documents from electronic libraries and repositories. 
* Version 1.1

### How do I get set up? ###

* Summary of set up
    - In a way, it is the sum of hardware plus software
* Configuration
    - _In the making_
* Dependencies
    - Vide [Colophon.md](https://bitbucket.org/imhicihu/ai-document-recognition/src/bf596eb94e36bd181d31c8befb9e88c6fe31924d/Colophon.md?at=master&fileviewer=file-view-default)

### Source ###

* Check them on [here](https://bitbucket.org/imhicihu/ai-document-recognition/src)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/ai-document-recognition/issues)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/ai-document-recognition/commits/) section for the current status

### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/ai-document-recognition/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/ai-document-recognition/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### License ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png) 