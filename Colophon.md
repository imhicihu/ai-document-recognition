## Technical requirements ##

* Hardware
     - Macbook 13 "
     - Macbook 15 "
     - iMac 21.5"
* Software
     * Environmental setting up (initial)
       	  - [Talus](https://github.com/juanbrujo/Talus): `bash` script to automate download & setup your Mac OS X development environment
     * Metadata management
          - [Apache Tika](https://tika.apache.org/): detects and extracts metadata and text from over a thousand different file types
     * OCR (Optical character recognition)
          - [Tesseract OCR](https://github.com/tesseract-ocr/tesseract): command line optical character recognition software
               * Training files (take in count the Castillian and French, Middle [_ca._ 1400-1600] example). More data can be found [here](https://github.com/tesseract-ocr/tesseract/wiki/Data-Files)
               * Take in count the Tensorflow chapter. More data can be found [here](https://github.com/tesseract-ocr/tesseract/wiki/NeuralNetsInTesseract4.00)
     * GIT workflow
          - [Github Downloader](https://github.com/intezer/GithubDownloader): ~~datasets &~~ repository downloader
          - [SourceTree](https://www.sourcetreeapp.com/): GIT client
     * Deployment diagram
          - [Plantuml](http://www.plantuml.com/plantuml/uml/):  Diagram / deployment diagram / critical path
     * Code editor
          - [Aquamacs Emacs](http://aquamacs.org/download-release.shtml): Editor for Text, HTML, LaTeX, C++, Java, Python, R, Perl, Ruby, PHP, and more...
          - [Atom editor](http://atom.io): code multi-language editor
     * Code sharing
          - [Carbon](https://carbon.now.sh/): (Automatization on code screen sharing)
     * Text handler
          - [Bean](http://www.bean-osx.com/Bean.html): word processor for MacOSX
     * Digital documentation
     	  - [ScanBot](https://scanbot.io/en/index.html): scan, edit and manage all of your paperwork
     * Image editor
          - [ImageOptim](https://github.com/ImageOptim/ImageOptim): image optimization
          - [Filmulator](https://github.com/CarVac/filmulator-gui): RAW format editor
     * Python script
          - [Augmentator](https://github.com/mdbloice/Augmentor): Augmentor is a Python package designed to aid the augmentation and artificial generation of image data for machine learning tasks. It is primarily a data augmentation tool, but will also incorporate basic image pre-processing functionality. More data can be found [here](http://augmentor.readthedocs.io/en/master/)
     - Cleaning database:
          - [OpenRefine](http://openrefine.org/): cleaning tool for datasets and datatables
          - [Talent open studio for data quality](https://www.talend.com/products/talend-open-studio/data-quality-open-studio/)
          - [Talend Data Preparation Free Desktop Edition](https://www.talend.com/products/data-preparation/data-preparation-free-desktop/)
     - Text Converter:
          - [Pandoc](https://github.com/jgm/pandoc/)
     - Bibliographic searcher:
        - [ScienceFair](http://sciencefair-app.com): Discover, collect, organise, read and analyse scientific papers
     - Terminal:
        - [Hyper](https://hyper.is/): Terminal console for the MacOSX

* Documentation
     - [Developer certificate](https://developercertificate.org/)

* Online tools
     - Plugins (curated and discriminated by topics)
        - [Awesome design plugins](https://flawlessapp.io/designplugins)
     
     
## Legal ##

* All trademarks are the property of their respective owners.