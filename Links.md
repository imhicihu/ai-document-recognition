## Links

* [Artificial Intelligence Is Cracking Open the Vatican's Secret Archives](https://www.theatlantic.com/technology/archive/2018/04/vatican-secret-archives-artificial-intelligence/559205/)
* [Machines reading the archive: handwritten text recognition software](https://blog.nationalarchives.gov.uk/blog/machines-reading-the-archive-handwritten-text-recognition-software/)
* [Hand-written digit recognition using an artificial neural network (ANN) and LabVIEW image processing](https://forums.ni.com/t5/General-Academic-Projects/Hand-written-digit-recognition-using-an-artificial-neural/ta-p/3513227)
* https://www.himanis.org/
* [In Codice Ratio](http://www.inf.uniroma3.it/db/icr/index.html)
* [In codice ratio github repo](https://github.com/Kidel/In-Codice-Ratio-OCR-with-CNN) & [here](https://github.com/Kidel/In-Codice-Ratio-OCR-with-CNN)
* [IBM Code Model Asset Exchange: Spatial Transformer](https://github.com/IBM/MAX-Spatial-Transformer-Network)
![spatial-transformer.png](https://bitbucket.org/repo/8zyLxnx/images/2940267345-spatial-transformer.png)
* [Augmentor](https://github.com/mdbloice/Augmentor): Image augmentation library in Python for machine learning.
	 - [Official Repo Documentation](https://augmentor.readthedocs.io/en/master/)
* [Google query](https://www.google.com.ar/search?q=machine+learning+on+medieval+books+recognition+ocr&oq=machine+learning+on+medieval+books+recognition+ocr&aqs=chrome..69i57.23047j1j4&sourceid=chrome&ie=UTF-8)
* [Deep-Learning-CNN-for-Image-Recognition](https://github.com/Kidel/Deep-Learning-CNN-for-Image-Recognition)
* [Fonts for Tesseract training](https://github.com/UB-Mannheim/tesseract/wiki/Fonts-for-Tesseract-training)
* [Handwritten Korean Character Recognition with TensorFlow and Android](https://github.com/IBM/tensorflow-hangul-recognition)
* [Google Patents](https://github.com/google/patents-public-data/tree/master/models/landscaping): machine learning plus some enhancements (metadata)
* [Identify information in document images - A composite code pattern](https://github.com/IBM/image-recognition-and-information-extraction-from-image-documents): take in count the _inner_ links 

## Typography & fonts
* [On the Origin of Patterning in Movable Latin Type](https://www.lettermodel.org/): inspiration to adapt (and digitize a medieval typography)

## Miscellaneous

### Augmentation
* [Augmentator](https://github.com/mdbloice/Augmentor): couple of python scripts to augment the size of groups of images in bulk form. Take in count!

### Python scripts
```
* http://scikit-image.org/docs/stable/auto_examples/features_detection/plot_template.html#sphx-glr-auto-examples-features-detection-plot-template-py
* http://scikit-image.org/docs/stable/auto_examples/features_detection/plot_censure.html#sphx-glr-auto-examples-features-detection-plot-censure-py
* http://scikit-image.org/docs/stable/auto_examples/filters/plot_nonlocal_means.html#sphx-glr-auto-examples-filters-plot-nonlocal-means-py
```