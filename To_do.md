* https://github.com/ICIJ/extract  (parallelised content extraction and analysis) *Experimental* from the International Consortium of Investigative Journalists
* http://www.inf.uniroma3.it/db/icr/publications.html
* pdf.js (experimental): pdf viewer > https://github.com/mozilla/pdf.js
* [Spatial Transformer Networks](https://github.com/tensorlayer/tensorlayer/tree/master/examples/spatial_transformer_network) and [here](https://github.com/zsdonghao/Spatial-Transformer-Nets)