* [Latin OCR for Tesseract](https://ryanfb.github.io/latinocr/)
* [Lace (greek ocr project)](http://heml.mta.ca/lace/index.html)
* [IBM'S Identify information in document images - A composite code pattern](https://github.com/IBM/image-recognition-and-information-extraction-from-image-documents)
* [Github Handwriting recognition](https://github.com/viritaromero/handwritingrecognition)
* [Template matching data tables](http://cobecore.org/blog/template-matching/)
* [PyPDFOCR - Tesseract-OCR based PDF filing](https://github.com/virantha/pypdfocr): Take a scanned PDF file and run OCR on it (using the Tesseract OCR software from Google), generating a searchable PDF
* [Leptonica Library](https://github.com/danbloomberg/leptonica): Location of baselines and local skew determination, skew determination of text images plus some another image enhancements: dewarping, unskew, _et alia_
* [hOCR](https://en.wikipedia.org/wiki/HOCR): ocr normative and good practices
* [machine learning & typography hacks](https://hackmd.io/@woErdZ3qT46sTD-YLjyFrw/B1BZCZpPX?type=view)